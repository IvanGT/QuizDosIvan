﻿using QuizDosIvan.NewFolder;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuizDosIvan
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
        
        public void ListFood(object sender, EventArgs e)
        {
           
            Comida crear = new Comida()
            {
                nombreComida = EntryFoodDos.Text,
            };
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlbd))
            {
                // crear una tabla en base de datos.
                connection.CreateTable<Comida>();
                // crear registro  en la tabla "tarea"
                var result = connection.Insert(crear);
                if (result > 0)

                {
                    DisplayAlert("correcto", "la tarea se creo correctamente", "ok");


                }
                else
                {
                    DisplayAlert("Error", "La Tarea No Fue Creada", "ok");
                }

            }

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlbd))
            {
                List<Comida> listaComida;
                listaComida = connection.Table<Comida>().ToList();

                listView.ItemsSource = listaComida;
            }
        }




    }
}
