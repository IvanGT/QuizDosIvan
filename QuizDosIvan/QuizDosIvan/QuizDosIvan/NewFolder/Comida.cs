﻿using SQLite;


using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDosIvan.NewFolder
{
    class Comida
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        // el nombre no mayor de 150 digitos
        [MaxLength(150)]
        public string nombreComida { get; set; }

    }
}
